import json
import requests
import urllib.request
import os
import shutil
import datetime
r = requests.get('http://pawoo.net/api/v1/streaming/public', stream=True)

def img_download(url,save_path):
    res = requests.get(url,stream=True)
    with open(save_path,"wb") as fp:
        shutil.copyfileobj(res.raw,fp)

def make_save_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
for line in r.iter_lines():

    # filter out keep-alive new lines
    if line:
        now_time = datetime.datetime.now().strftime("%Y%m%d%H")
        normal_dir = "img/normal/{}".format(now_time)
        sensitive_dir = "img/sensitive/{}".format(now_time)
        decoded_line = line.decode('utf-8')
        if "data" in decoded_line:            
            try:
                decoded_line = decoded_line[6:]
                if decoded_line.isdecimal():
                    continue
                json_data = json.loads(decoded_line)
                for media in json_data['media_attachments']:
                    url = media["url"]
                    file_name = os.path.basename(url)
                    
                    make_save_dir(normal_dir)
                    make_save_dir(sensitive_dir)

                    if json_data["sensitive"]:
                        img_download(url, "{}/{}".format(sensitive_dir,file_name))
                    else:
                        img_download(url, "{}/{}".format(normal_dir,file_name))
            except Exception as e:
                print(e,decoded_line)

        